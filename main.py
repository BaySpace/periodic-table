
from mendeleev import element
import random
import csv
import time


def elementAttributes(atomicNumber): #Creates a dictionary of attributes related to the element put into the function
    # @Future Me: Create from list instead
    dict = {}
    e = element(atomicNumber)
    dict["Name"] = e.name
    dict["Atomic Number"] = str(atomicNumber)
    dict["Weight"] = e.atomic_weight
    dict["Boiling Point"] = e.boiling_point
    dict["Density"] = e.density
    dict["Evaporation Heat"] = e.evaporation_heat
    dict["Proton Affinity"] = e.proton_affinity
    dict["Electron Affinity"] = e.electron_affinity 
    dict["Heat of Formation"] = e.heat_of_formation 
    return dict

def randomElement(): #Lower range means fewer elements with replaced None values. Mendeleev library has 118 elements
    i = random.randint(1,118) 
    return i



def hiddenPowers(atomicNumber, luckNumber=1): 
    #Will determine the actual power levels of this card. HP, Strength, Defense.
    #Due to the possible randomization on alot of variables, some cards could be OP af. 
    card = elementAttributes(atomicNumber)
    powers = {}
    for key in card:
        if card[key] != None:
            powers[key] = card[key]
        else:
            powers[key] = random.randint(2, 1000) #
    powers["Luck"] = random.randint(1, luckNumber) #Luck is super hidden and based on input, default is 1

    hitpoints = powers["Weight"] * 0.15 + powers["Density"] * 0.3 + powers["Heat of Formation"] * 1.3 + 10 * powers["Luck"]
    strength = powers["Proton Affinity"] * powers["Luck"] - powers["Electron Affinity"] * powers["Luck"] / 2
    defense = powers["Boiling Point"] * 1.5 + powers["Density"] * 2 + powers["Evaporation Heat"] * 0.001

    return hitpoints, strength, defense
            

#Returns the finalized card as a dict with super complex algo applied
def elementCard(atomicNumber):
    #Visible elements:
    uniqueCard = {}
    e = elementAttributes(atomicNumber)
    ez = 0
    for key in e:
        if ez < 2: #Amount of visible attributes; rest are used for hidden power multipliers
            uniqueCard[key] = e[key]
            #print(key+":", e[key])
            ez += 1
    balanceAttribs = ["hp", "strength", "defense"]

    balanceAttribs = hiddenPowers(int(e["Atomic Number"]))
    uniqueCard["Hitpoints"] = format(balanceAttribs[0], "1.0f")
    uniqueCard["Strength"] = format(balanceAttribs[1], "1.0f")
    uniqueCard["Defense"] = format(balanceAttribs[2], "1.0f")
        
    return uniqueCard

    # nice
def writeDeck(fileName, amountOfElements=118): #Library has 118 total elements total, we're going for a full deck per default
    x = 1
    card = elementCard(x)
    deckDict = {}
    with open(fileName, "w") as csvOutput:
        pass
    keyList = ['Name', 'Atomic Number', 'Hitpoints', 'Strength', 'Defense']
    with open(fileName, "a") as csvOutput:
        writer = csv.DictWriter(csvOutput, fieldnames=keyList)
        writer.writeheader()
        while x <= amountOfElements:
            loopCard = elementCard(x)
            writer.writerow(loopCard)
            x += 1


def main():
    s = time.time()
    #print(s)
    writeDeck("data.csv")
    e = time.time()
    print("Time elapsed:",format(e-s, "0.2f")+"s")
main()