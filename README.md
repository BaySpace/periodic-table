# README #

This README gives an overview of the process we are in to develop this marvelous piece of software!

### What is this repository for? ###

* A Periodic table with certain "power" values assigned to each element: Hitpoints, Strength and Defense
* These values are generated using some very complex and indeed extremely scientific algorithms developed by our in-house team of developers.
* What it also means is that each run of these so-called algorithms generate a completely unique set of all elements known to man.
* In this new table we have the name and atomic number which remain the original, i.e. Oxygen is still known as 'O' and it has the atomic number of 8.
*   Though grounded in actual, real-life values, the hp, str and def values of any card on any run have no real value by themselves
*   To give them value, we need to collect the data and somehow understand it; probably by visualizing, tweaking super complex AI algirothm, etc etc
* Over a lot of runs you will see this element, Oxygen, is perhaps not as volatile as others...
* Oxygen is not really that volatile in real life, so its power levels of hp, str and def remain somewhat stable through runs...
* The further you go down the list, the more volatile these powers become; sometimes you may find overwhelming strength -
* And sometimes you could find a card with a fountain of hitpoints only to realize your Strength is -238
* 
* 
* This means if you can store each run, suddenly data analysis becomes exciting 
* It means we can gather some data on the chemical elements of the world either from wiki or a handy library
* 
* 
### How to use ###
*

* First you will need to install the library "mendeleev"
*       pip install mendeleev
* 
* Now to use this very scientific tool, all you have to do is execute this command: 
*       python main.py 
* 
* You will magically receive a file called "data.csv"
* Congratulations!

### Application ###

* Needs to fetch general data of elements from https://en.wikipedia.org/wiki/List_of_chemical_elements or find a fitting library
* This will be used to build a dictionary of the elements with only name, symbol, atomic number (subject to change)
* A whole "deck" of elements will be created, somewhat like cards
* Using the dictionary we can look up info of the element on its wiki page e.g. https://en.wikipedia.org/wiki/Silver
* Core function is to return the atomic weight of the element
* Other functions could be implemented and is probably as easy as the core function to implement
* The data collected on the elements will be saved locally in a JSON file, though .csv could make sense
* JSON is great because it is pretty much a python dictionary in file format with wide compatibility options
* csv makes more sense for data analysis
* This little application could in turn be used as the backbone of a more complex app based on elements
* Maybe the aim is to create a card game of sorts or even an actual NFT!

### To do ###
* Need to make it stop skipping a line on export
* Some cleanup is needed though comments are ok ish
* 

### Closing thoughts ###
* Seems cool that you can create a list of 118 unique elements and analyze it
* This could help tweak the super complex algorithms involved in calculating the innovative stats system
* Definitely seems possible to expand this into a proper database 
* You should be able to buy your own favorite NFT cards with unique powers on your favorite marketplace within a month

### Who do I talk to? ###

* Kian Bay - kianbay@gmail.com // kian.dk (not really)
* Victor F R Runge - victorfrunge@gmail.com // holddetægtestepbror.dk (yes, really!)